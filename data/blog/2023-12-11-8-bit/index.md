I was 5 years old when an MSX computer landed in our home. The clerk at the
electronics shop convinced my parents that it would make my older brother excel
at maths thanks to a videogame that, essentially, would ask you arithmetic
questions and let you play simple games in return for a bunch of responses
gotten right. The truth was, of course, that it was way better to just load any
other game that could be played without demanding any previous intellectual work
from you. And that's what we did day in and day out for many years.

Here's the oldest picture I could find of me -for now forgotten reasons in a cat
costume- and our flashy computer setup.

![A little kid dressed in a cat costume in front of an MSX computer](msx-cat.jpg)

One of the greatest things about these old systems was their *lack* of an
operating system. They would present you with a BASIC interpreter prompt right
after booting -which took under a second-, and you would be forced to at least
learn a couple of programming commands to get them to do anything for you.

Eventually, one would get curious and start dabbing a bit into BASIC. I wrote my
first programs in that computer, which originally just lived in the volatile
memory of the machine and would die after power off. At one point my programs
were large enough to justify the work of writing down the whole thing on a piece
of paper to be able to recreate it later. Much, much later, I found out that the
cassette recorder could not only load but also store programs into tapes.

These explorations gave me an idea of how the logic of the games I was playing
may work, but how they achieved such marvelous graphics (and music!) remained a
mystery to me.

Here's a funny side story. Maybe 15 or 20 years after having put my MSX computer
in a cabinet to rest, I found out that the machine could actually do color
graphics. I wanted to boot the computer to see if it still worked, and I could
not find our old green phosphorous monochrome monitor anywhere, so I figured I
would try plugging the MSX into a TV screen. Needless to say, after I was
greeted by an MSX logo **in color**, I spent all summer trying out all the games
from my childhood to see what they *really* looked like.

I've now been in the field of educational programming for many years, where we
try to make all things about computing more accessible to learners. Quite often
I'll stumble into an old algorithm, concept or computational idea that I do not
know much about. Then, I'll start trying to first dissect it, and later abstract
it, to make it available for others to understand.

In this occasion, it was low-bit graphics. I wanted to know how these old games
managed to pack so much artwork into so little memory, I wanted to know how they
stayed so fast and responsive, and I wanted to know if MicroBlocks could help me
resurrect these almost forgotten techniques.

We had just recently launched [Boardie](../2022-12-07-boardie-intro/), the
MicroBlocks virtual microcontroller, and it proved to be a perfect platform to
experiment with old school graphics. I spent a while making a new *8 Bit
Graphics* library for MicroBlocks, but sadly life and work happened and I never
documented my work.

That is, until now. If you too are curious about the way these old systems
achieved their graphics, you may want to follow along the new activity in our
[Learn](https://learn.microblocks.fun) page:

[Old School Graphics
activity](http://learn.microblocks.fun/en/activities/old-school-graphics-en/)

I hope that you'll enjoy the journey as much as I did!
