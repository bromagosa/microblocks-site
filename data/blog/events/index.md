## Upcoming Events
- February 1-2, 2025: MicroBlocks will be present at [FOSDEM](https://fosdem.org/). The team will be doing presentations and workshops for children during [FOSDEM Junior](https://fosdem.org/2025/schedule/track/junior/).
- May 7-14, 2025, [Robotics and programming for education](https://faberllull.cat/en/residencia.cfm?id=44743&url=robotics-and-programming-for-education-2025.htm), Olot, Spain

## Past Events
- December 8, 2024: John Maloney will present an introductory workshop via Zoom to the [Logo Foundation](https://el.media.mit.edu/logo-foundation/services/microblocks.html)
- December 4-6, 2024: Kathy Giori will be present at the [TechWomen Alumnae Summit](https://www.techwomen.org/get-involved/techwomen-alumnae-summit) in Morroco.
- October 18-20, 2024: Kathy Giori will be present at the MicroChip booth during the [Maker Faire Bay Area](https://makerfaire.com/bay-area/)
- September 14-15, 2024: MicroBlocks stand at Maker Days Eindhoven, The Netherlands: MakerDays is a festival of discovery and inspiration, an enthusiastic celebration of ’the world of making’. You will meet people from far and wide who will show the products of their creativity, such as: laser cutting & 3D printing, new fashion trends, robotics, animatronics, etc. [Maker Days Eindhoven](https://makerdays.nl/)
- August 15 BADGE: Intro to Electronics & Microcontrollers with MicroBlocks [MAKE Santa Fe 2024](https://makesantafe.org/event/intro-to-robotics-explore-the-world-of-physical-computing/). Join us in discovering microcontrollers and the nuances of the digital world, how things are integrated and automated, and how we can use the tools to exert control over our environment.
- June 3 Robotics for TEENS: Explore the World of Physical Computing with MicroBlocks [MAKE Santa Fe 2024](https://makesantafe.org/event/robotics-for-teens-explore-the-world-of-physical-computing-with-microblocks/). Join us in discovering microcontrollers and the nuances of the digital world, how things are integrated and automated, and how we can use the tools to exert control over our environment.
- May 30, 2024: Tilburg University, The Netherlands: MicroBlocks Workshop during the yearly Science Orientation Netherlands meeting. It is a community of schools that together give science orientation an explicit place in the curriculum.
- April 18 2024: Avans University Breda, The Netherlands, Innovative Studio Workshop. Avans Innovative Studio is a new educational concept. You design tangible products at high speed. To do this, you first determine the core of the question and then the solution direction.
- March 2, 2024: Tilburg, The Netherlands: MicroBlocks Workshop during the PartyDojo celebrating the 10 year anniversary of CoderDojo Netherlands.
- February 3-4, 2024: MicroBlocks at [FOSDEM 2024](https://fosdem.org/2024/). Featuring [presentations](https://fosdem.org/2024/schedule/track/educational/) and [workshops for young people](https://fosdem.org/2024/schedule/track/junior/).
- November 26, 2023: Vught, The Netherlands: MicroBlocks stand during [Vught Digitaal](https://www.codekids.nl/vd2023/)
- November 25, 2023: Royal Library The Hague, The Netherlands: MicroBlocks Workshop during DojoConNL, the yearly gathering of CoderDojo Champions and Mentors. [CoderDojo](https://coderdojo.com/en/)
- July 31-August 4, 2023: CSforMA Professional Development Workshop, Woburn, MA
- May 6, 2023: Robolot Conference, Olot, Spain
- May 4-11, 2023: Faber Robotics Residency, Olot, Spain
- Apr 25-29, 2023: TechWomen MicroBlocks Workshops led by Kathy Giori, Kazakhstan
- April 21-22, 2023: TechWomen MicroBlocks Workshops led by Kathy Giori, Kyrgyzstan
- April 19-20, 2023: TechWomen MicroBlocks Workshops led by Kathy Giori, Uzbekistan
- February 5, 2023: MicroBlocks presentation at [FOSDEM 2023](https://archive.fosdem.org/2023/)
in the FOSS Educational Programming Languages devroom on Sunday. Here is the
[devroom schedule](https://fosdem.org/2023/schedule/track/foss_educational_programming_languages/).
- August 1-5, 2022: CSforMA Professional Development Workshop, Burlington, MA
- July 25-29, 2022: CSforMA Professional Development Workshop, Burlington, MA
