## Using MicroBlocks with MIT App Inventor

Imagine what you might do with mobile apps that interact with the physical world.
You might control a robotic dog or a spooky Halloween light display from your phone.
Or an app might tell you when your house plants were getting
thirsty and let you water them with the click of a button.

Projects like these are easy to create with MicroBlocks and the new MicroBlocks extension for MIT App Inventor.

MIT App Inventor is a free, blocks programming language that makes it easy to
create mobile apps for Android phones and tablets. (The App Inventor team is working to add iOS support as well.
However, at the moment, the MicroBlocks
extension works only with Android devices.)

The key to making mobile apps that interact with the physical world is
the new MicroBlocks MIT App Inventor extension, which uses Bluetooth Low Energy (BLE)
to communicate between an Android mobile device and BLE-capable MicroBlocks boards such
as the micro:bit V2.

Implementing such apps requires creating both an App Inventor project
and a MicroBlocks project. Fortunately, it doesn't take much code to do cool
things, and it is easy to get started. If you are already familiar either MIT App
Inventor or MicroBlocks, you're already halfway there!

### MicroBlocks Hello Purr

*MicroBlocks Hello Purr* is a simple demo application created by Natalie Lao
from the [App Inventor Foundation](https://www.appinventorfoundation.org/).
It allows you to send *purr* messages between your mobile
device and a micro:bit V2. Although this application requires very little code,
it demonstrates the three key elements of every MicroBlocks/App Inventor project:
making a Bluetooth (BLE) connection, sending messages, and handling incoming messages.

![HelloPurr Phone Screen](HelloPurrScreen.jpg)

### MicroBlocks Scripts

The MicroBlocks side contains three scripts:

![MicroBlocks Scripts](HelloPurrMicroBlocksScripts.png)

The startup script displays an image (a line of LED's) on the LED display to show that it is running.

Pressing the A button sends the message *PET* to App Inventor. It also scrolls the word *Purr* as feedback.

The third script runs when MicroBlocks receives the *petted* message from App Inventor.
It displays a cat face and uses the tone block in two **for** loops to create a purr sound.

You can make MicroBlocks respond to more messages by adding additional
**when _ received** blocks.

### App Inventor Scripts

The App Inventor side has four scripts. The first two are used to initiate a BLE connection
when the connect button is pressed and to turn the button green to provide feedback that a connection has been made:

![AI Connection Scripts](AppInventorConnectScripts.png)

The connection is made by name. MicroBlocks boards have names like *MicroBlocks XYZ*,
where the last three letters identify the board. Your micro:bit will flash
its three-letter code when it is reset or powered up. When using the app, you'll need
to change the last three letters of the board name
in the input box to your board's three-letter code using capital letters.

[*Note: If your board is based on a Raspberry Pi Pico-W, then it's name will start with
the word "Pico" instead of "MicroBlocks". Technical limitations of the
BLE support on the Pico-W limits the length of board names to eight characters.*]

The other two App Inventor scripts handle sending and receving messages:

![AI Message Scripts](AppInventorMessageScripts.png)

Clicking the cat picture sends the message *petted* to MicroBlocks.

Receiving the message *PET* from MicroBlocks plays a purr sound on the mobile device.

You can probably see how to extend this App Inventor project by adding buttons to send additional messages.
To handle additional incoming messages, you just need to add an *if* block for each message.

### Complete Programs

Download the complete programs to try this yourself.

App Inventor: <a href="./MicroBlocksHelloPurr.aia" download>MicroBlocksHelloPurr.aia</a>
<br>
MicroBlocks: <a href="./MicroblocksHelloPurr.ubp" download>MicroblocksHelloPurr.ubp</a>

### Resources

[App Inventor](https://appinventor.mit.edu)
<br>
[MicroBlocks](https://microblocks.fun)
<br>
[App Inventor MicroBlocks BLE Extension](https://community.appinventor.mit.edu/t/microblocks-ble-extension/129412)
<br>
[MicroBlocks Extension Documentation](https://wiki.microblocks.fun/en/appinventor/ai2extension)
