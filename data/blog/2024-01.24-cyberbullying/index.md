# Using MicroBlocks, OctoStudio and DotPack for a dynamic against bullying and cyberbullying

by Edulab team at Citilab

**Citilab** is undertaking [a project](https://www.citilab.eu/projecte/salut-digital/) aimed at enhancing the digital health of citizens. For this project, conferences have been scheduled, collaboration with experts is underway, and game-based dynamic lessons are being designed and implemented to reach diverse groups of people.

One of the recent activities involved partnering with a local basketball club to address the issues of bullying and cyberbullying. The young participants engaged in various activities to explore and address this problem, acquiring resources to prevent such situations.

![](bullying4.jpg)

For one of these activities, a backpack was used to physically represent the challenges posed by cyberbullying and illustrate how it affects normal activities. Shooting baskets represented a normal activity and the weight of the backpack represented the burden of cyberbullying.

![](bullying2.jpg)

If the backpack is emptied of the negative emotions and the metaphorical weight caused by these situations, everything works better, and shooting hoops becomes easier.

![](bullying3.jpg)

In the video review of [Citilab's activities in 2023](https://youtu.be/O2vs9kbueuE?si=xcZJxXjgJO2pKY7f&t=256) there is a brief fragment related to this dynamic.

## MicroBlocks, dotPack and OctoStudio

The backpack was special. It is a [dotPack](https://dotpack.fun) backpack that combines a panel of 16 x 16 RGB LEDs (**Neopixels**) with an ESP32 controller. The Scratch-like **MicroBlocks** programming environment allows the backpack to be be easily programmed. In this case, a sad face lit up when the backpack was loaded with problems, and a happy face lit up when the young people had freed themselves from them.

![](dotpack.gif)

We created a mobile phone application to allow the facilitators of the activity to easily change the faces. For this purpose, [OctoStudio](https://octostudio.org) was used. OctoStudio was created by the MIT Media Lab's Lifelong Kindergarten team, the same group that created Scratch. It is a beginner-friendly programming language for phones with the ability to "beam" messages to **MicroBlocks**. It is not necessary to pair the devices; the communication is done using Bluetooth advertising.

The OctoStudio application has two button sprites: a happy face that beams a "heart" beam and a sad face that beams a "square". The application also sends a "star" beam when the phone is shaken.

![](octostudio.jpg)

The **MicroBlocks** part was easy to implement using the new **OctoStudio** library. The program waits until a beam is received and illuminates the **Neopixel** panel with the appropriate bitmap face:

![](microblocks.png)

The bitmap files for this example, can be downloaded [here](https://cloud.citilab.eu/s/sbjBmycRaFSB68c).

The results of the activity were very positive. The young participants learned how to detect and address cyberbullying behaviors and were highly intrigued by the operation of the backpack. Now that they are interested, we have the opportunity to offer them workshops introducing programming with **MicroBlocks** and **OctoStudio**, allowing them to understand the functioning of the backpack and write their own programs to control it.

## Acknowledgments

- MicroBlocks Team (especially to John Maloney and Wenjie Wu)
- OctoStudio Team
- Yi Li from DotPack
