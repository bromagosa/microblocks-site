## First Time at FOSDEM for MicroBlocks and Me

By Kathy Giori @kgiori

Last June, MicroBlocks fan [Peter Mathijssen](https://twitter.com/PeterM66) planted the seed of an idea that we should submit a proposal to organize a [FOSS Educational Programming Languages](https://fosdem.org/2023/schedule/track/foss_educational_programming_languages/) devroom for the return of on-site [FOSDEM](https://fosdem.org/2023/) at the Universite Libre du Brussels (ULB), Feb 4-5, 2023. There hadn’t been an in-person gathering since 2019. Although FOSDEM had been on my bucket list for a while, I had never been, so I raised my hand when Peter asked for a co-manager of a devroom proposal.

![peter-kathy](i1.jpg)

Using the network of a few of our experienced educational programming tool friends, we were able to quickly assemble a list of successful projects. We had no problem finding speakers to fill the half-day devroom we were approved to organize, and could have easily filled a full day.

For the MicroBlocks [presentation](https://fosdem.org/2023/schedule/event/microblocks/), I partnered with developer [Bernat Romagosa](http://romagosa.work), a team member of SAP’s amazing Young Thinkers program led by [Christiane Bauer](https://people.sap.com/christiane.bauer). I began the talk by reviewing the four pillars that make MicroBlocks special: it’s live, parallel, autonomous, and portable. Then I handed Bernat a board that I had used for my demo. He plugged it into his laptop to show the ease of sharing. By selecting “Open from board”, his MicroBlocks application proceeded to load my program into his IDE. Without changing any of the scripts, he then opened the virtual hardware we call “[boardie](https://microblocks.fun/blog/2022-12-07-boardie-intro/)” (that is available in the [browser version](https://microblocks.fun/run/microblocks.html)), and the same code ran just fine.

![kathy-bernat](i2.jpg)

Then Bernat continued with a demo that was borderline magic. He fetched a stack of credit/debit and other RFID-enabled smart cards and a key fob from his pocket and laid them out on the table in front of him. Using a battery-powered [M5Stack](https://shop.m5stack.com/) with I2C-connected [RFID-reader](https://shop.m5stack.com/products/rfid-sensor-unit?variant=16804784701530), he “sensed” the unique code of the cards, and while doing so, the M5Stack produced a different tone for each card. He then played a short musical ditty by waving his wireless music-making wand over the various cards laid out like fat piano keys on a desk. It was a definite crowd-pleasing demo!

A bonus exposure opportunity for MicroBlocks was our ability to demonstrate the fun of physical computing on one side of the [Software Freedom Conservancy](https://sfconservancy.org/) (SFC) stand. We showed an array of boards that MicroBlocks supports (e.g., [Robotistan](https://www.robotistan.com/) [PicoBricks](https://picobricks.com/), [M5 Core](https://shop.m5stack.com/collections/m5-controllers/CORE), [M5 Atom](https://shop.m5stack.com/collections/m5-controllers/ATOM), [Elecrow Mbits](https://www.elecrow.com/mbits.html), [Elecfreaks Pico:ed](https://shop.elecfreaks.com/products/elecfreaks-pico-ed-v2), Adafruit [Circuit Playground Express](https://www.adafruit.com/product/3333) and [Bluefruit](https://www.adafruit.com/product/4333) version, and Adafruit [Clue](https://www.adafruit.com/product/4500)). A couple are so new that few recognized them. We attracted attention and delighted the attendees with live demos. For one demo I used [micro:bit](https://microbit.org/) boards assembled in two ElecFreaks [Ringbit:cars](https://www.elecfreaks.com/ring-bit-car-v2-for-micro-bit.html) to show “dancing” robots (using another micro:bit to wirelessly control them in sync). I also demo’d and then encouraged the younger attendees to try out my ESP32-powered touch screen. You could draw on it with your finger (like digital finger-painting), then press slightly harder to erase the screen and start over. Peter showed off his ElecFreaks [Wukong](https://www.elecfreaks.com/elecfreaks-wukong2040-breakout-board-for-raspberry-pi-pico.html) board, assembled into a Lego robot car with an ultrasonic distance sensor in front. He had programmed it so that it would continually move around while turning as necessary to avoid obstacles directly in front of it.

![robots](i3.jpg)

![boards](i4.jpg)

We imagined how lovely it would be if next year we could convince the FOSDEM organizers to offer us a space to set up a “youth zone” or “physical computing zone” with plenty of hands-on hardware that attendees could experiment with, and to try their hand at MicroBlocks. With such an amazing tool to program today’s great hardware, you can create something practical, beautiful, or even magical.

It was my first time to FOSDEM, my first time to Brussels, but most hopefully not the last!

![brussels](i5.jpg)
