## Making art with MicroBlocks

I love the screen of my [PicoBricks](https://picobricks.com/), my [Adafruit CLUE](https://www.adafruit.com/product/4500), my [M5Stack Core](https://shop.m5stack.com/products/basic-core-iot-development-kit?variant=16804801937498) and my [Citilab ED1](https://market.citilab.eu/en/producte/ed1-board/). I also love to work with [Boardie](https://microblocks.fun/blog/2022-12-07-boardie-intro/).

I also love to make images using blocks. What does MicroBlocks have to offer?

There are several graphical libraries you can use:

- The TFT library
- The TFT Turtle library
- The [OLED library](https://wiki.microblocks.fun/en/extension_libraries/oled)
- The OLED Turtle library
- The [8 Bit Graphics library](https://microblocks.fun/blog/2023-12-11-8-bit/)

I explored every one of them and for these Activity Cards I settled for the TFT and the 8 Bit Graphics library.

The TFT library has blocks to make lines, circles, rectangles and triangles, you can change colors and write text on the screen.

The 8 bit Graphics library gives you the possibility to make [sprites](https://learn.microblocks.fun/en/activities/old-school-graphics-en/) and manipulate them. You can change colors and size and you are able to rotate and/or mirror them.

Some Activity Cards are inspired by the work of real artists, but most of them came from my own imagination.

Below you can find some images with the result of the Activity Cards. All images are made using Boardie, because it is easier to save the result. You just have to right click on the image and save it.

### Color Maze

![image](colormaze.png)

### 10Print

![image](10print.png)

### Retro Circles

![image](retrocircles.png)

### Vasarely

![image](vasarely.png)

### Hexagon

![image](hexagon.png)

### Raza

![image](raza.png)

### Shemi

![image](shemi.png)

### Scramble

![image](scramble.png)

### Stella

![image](stella.png)

Boardie gives you an image of 240 x 240 pixels. In a lot of cases that is big enough, but sometimes that can be to small. What I sometimes do, is edit the image using [Gimp](https://www.gimp.org/) or enlarge it using [Upscayl](https://upscayl.org/). Both are open source.

An example, after using Upscayl, you can see below. The Shemi image is now 960 x 960 pixels. You can enlarge it even more.

![image](shemi_upscayl.png)

Here is a [link](https://drive.google.com/drive/folders/1em-MgIkgacuke_9M3qH-UiqSnfY8tjGI?usp=sharing) to the Activity Cards I made. You can also find the MicroBlocks project files there.

If you have any questions regarding these Activity Cards, or any other MicroBlocks related question, please join our [Discord Server](https://discord.gg/TCpHYbcvkS) and ask there.

