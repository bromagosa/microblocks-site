## Coming Soon: MicroBlocks 2.0!

The MicroBlocks team has been working hard on MicroBlocks 2.0, an exciting upgrade
with both visible changes and under-the-hood improvements.

### A New Look

The immediately visible change is a beautiful new UI designed by Marc Campreciós,
the talented UI/UX designer who created the MicroBlocks and Learn websites.

![MicroBlocks 2.0 UI, light mode](newUI.png)

The new UI also includes a "dark mode" option:

![MicroBlocks 2.0 UI, dark mode](darkMode.png)

A few parts of the UI, such as the dialogs for opening projects and importing libraries,
have not yet been updated. We will continue to work on those.

### Updated Block Design

In addition to the changes to the overall UI,
the graphic design of blocks has been updated to improve readability
while giving the blocks a clean, modern look. The block functionality has not
changed and the new blocks are close in appearance to the old design, so educators
can continue to use existing learning materials.
We realize it takes effort to update learning materials --
we certainly have plenty of our own materials to update! --
but we believe this new block design will carry us through the next decade of MicroBlocks evolution.

### More Space-Efficient Code

Under the hood, MicroBlocks 2.0 features a major change to the MicroBlocks virtual machine.

A virtual machine is a simulated computer with its own instruction set just like
a hardware CPU. The virtual machine architecture is the secret to liveness in MicroBlocks.
As you edit scripts, the scripts are incrementally compiled into virtual machine
instructions and sent to the microcontroller so they can be run right away.

Unfortunately, microcontrollers have limited code storage space. As MicroBlocks users
build larger and more complex projects, that space can fill up. A few robotics teams
in China hit that limit this past year. While we were able to expand the storage space
enough to allow them to continue, it is clear that MicroBlocks is getting used for
ambitious projects that are pushing the current code storage space limits.

MicroBlocks 1.0 used 32-bit virtual instructions; MicroBlocks 2.0 uses 16-bit ones.
That means the MicroBlocks 2.0 can store almost twice as much code in the same amount of code storage space.
Since code includes more than just instructions (e.g. string constants and meta data),
MicroBlocks 2.0 doesn't cut code size exactly in half, but the savings are substantial.

![Code size comparison table, 32-bit vs. 16-bit instructions](CodeSizeComparison.png)

Note: Since the new 16-bit instructions are different from the earlier 32-bit ones,
you'll need to update the firmware on your boards before you can use MicroBlocks 2.0.
Of course, it is good practice to keep the firmware on your boards up to date, but in
this case it is essential. If you connect a MicroBlocks 1.0 board to MicroBlocks 2.0 you
won't be able to program it. (Although you will be able to check firmware version number.)
Similarly, you can't program a MicroBlocks 2.0 board with old 1.0 version of MicroBlocks.

The MicroBlocks apps will warn you if the firmware on your board is not compatible with the app.

### Timeline and Transition

We plan to release MicroBlocks 2.0 around the end of October.

Before we release MicroBlocks 2.0, we will make a final stable release of MicroBlocks 1.2.
Thus, anyone not ready to switch to MicroBlocks 2.0 can continue to use that stable
release.

If you have any questions about MicroBlocks 2.0 or would like to help test the pre-release
version over the next several weeks, please join our [Discord Server](https://discord.gg/TCpHYbcvkS).
We'll be posting news and updates about MicroBlocks 2.0 there leading up to the release.
