## MicroBlocks 2.0 has Launched!

MicroBlocks Team have been working hard on MicroBlocks 2.0 for over a year.
For those eager to try it, it is now available as a pilot release.

### A New Look

The difference between MicroBlocks 1.2 and 2.0 is obvious at a glance.

Old MicroBlocks:
![Old MicroBlocks UI](oldUI.png)

New MicroBlocks:
![New MicroBlocks UI](newUI.png)

MicroBlocks 2.0 has a completely redesigned, modern user interface with clean icons,
an attractive color scheme, an animated blocks palette, and support for dark mode.
Block design changes include simpler outlines, better color contrast for improved
readability, larger data entry fields, and larger, more useable menu and option selection
triangles.

### Firmware Transition

MicroBlocks 2.0 also improves the virtual machine (VM), the firmware that executes MicroBlocks code.
The old VM used fixed-length 32-bit instructions; the new one uses variable-length 16-bit instructions.
This results in a substantial savings in code space.

Since they use completely different instruction sets, the two VM's are not
compatible. MicroBlocks 2.0 requires the new VM (firmware versions 300 and later)
while earlier versions of MicroBlocks require the old VM (firmware versions before 300).

There are no differences at the source code level; the same MicroBlocks
project and library files work in both MicroBlocks 2.0 and older versions.

The first time you connect a board with old firmware to MicroBlocks 2.0 you'll
be prompted to update the firmware:

![Update Firmeware Prompt](updateFirmware.png)

If you attempt to use a board with firmware that is not compatible with the version of
MicroBlocks you are using, you'll get this warning when you try to run code:

![Incompatible Firmeware Warning](incompatibleFirmware.png)

If you get that warning, you just need to update the firmware on your board using the
"update firmware on board" command in the gear menu. That will update the board with
firmware that matches the version of MicroBlocks you are using. You can use the same
technique to revert the firmware to use an older version of MicroBlocks, if desired.

### Stable Release Transition

After several weeks of testing MicroBlocks 2.0 as a pilot release,
the stable release will also be updated to MicroBlocks 2.0.

However, even after that, older versions of MicroBlocks will
available for download using the "Previous Versions" button
on the [Download page](https://microblocks.fun/download):

![Previous Versions Button](PreviousVersionsButton.png)

The last stable release of the old version is 1.2.100.

### MicroBlocks Chromebook App

Many educators, especially in the US, use the MicroBlocks Chromebook app.

Google has announced that they are
[discontinuing support for all Chromebook apps](https://support.google.com/chrome/a/thread/283077453/end-of-support-for-chrome-apps-and-how-to-launch-chrome-apps-after-that)
in January 2025. (Chromebook app support was discontinued for most users in June 2022
but an exception was made for Google's Enterprise and Education customers.)

Unless Google extends that deadline, the MicroBlocks Chromebook app will stop working in January.
Chromebook users will need to start using the MicroBlocks
[web app](https://microblocks.fun/run/microblocks.html).
MicroBlocks is a "progressive web app", which means that it can be installed as a application for offline use.

The Chromebook app is currently at 1.2.100, the final stable release of MicroBlocks 1.2.
Given that Chromebook app support is ending in January, we do not plan to update
the Chromebook app to MicroBlocks 2.0. However, MicroBlocks 1.2 has worked well in the
Chromebook browser and we expect MicroBlocks 2.0 to work just as well.

**Chromebook users, especially teachers:** Please let us know if you encounter any
issues switching from the Chromebook MicroBlocks app to running MicroBlocks in the browser.
We want to make it as easy as possible for you to use MicroBlocks.
