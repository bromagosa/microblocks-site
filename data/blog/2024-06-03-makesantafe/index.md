## MicroBlocks Classes at MAKE Santa Fe CLub, NM

By Turgut Guneysu @tguneysu

I was introduced to [MAKE Santa Fe](https://makesantafe.org/) through a resident friend of mine, Shawn Wallwork. Make Santa Fe supports weekend tinkerers, businesses developing prototypes, artists, inventors, and families & friends casually experimenting with new ideas. They provide a welcoming and inclusive environment for curious people ages 12 and older who would like to access a great range of tools in a comfortable and supportive setting.

When I first discussed the opportunity to setup a training program for MicroBlocks with their Operations Manager, [Christy Georg](https://makesantafe.org/maker/54340/), it was immediately obvious that their facilities and personnel would be a perfect fit for the task at hand. While the MAKE club had an earlier bout with trying to deliver training to young learners in the past, it was not very successful due to the hardships of coordinating class hardware and software requirements, as well as managing the short attention spans of the young attendees. It was no surprise that their initial expectations were kind of tempered by this past experience.

Starting in June 3, we first organized a two day training session for the potential teacher candidates. There were five attendees, all perfectly suited and experienced in working with microcontrollers and training students of various ages and backgrounds. 

![Teacher's Class](TC1.jpg)

They quickly took to working with MicroBlocks and were pleasantly surprised by the advanced capabilities supported. Programming many types 32-bit microcontrollers in real-time, capable of handling multitasking, interactivity and portability offered by the development environment quickly made full-fledged enthusiasts of them. After two days of training, they were perfectly comfortable with MicroBlocks and were looking forward to offering it as part of the computing curriculum.

The next step was to have a PR night for the community, to introduce the new training program. It was well attended in spite of the short notice, and resulted in filling up our first class offering to be taught by me. This would also serve as a guide session for their future offerings, as well as provide a means to discuss this experience and how it could further be customized for their vision of the training program.

![Class Announcement](MAKEClassDescr.jpg)

The first student trainig class was held the next day and was attended by five  students ages 12-16. [PicoBricks micro:bit Kit](https://picobricks.com/products/micro-bit-starter-kit?srsltid=AfmBOorm-noChl8G4V8jFYjilBWaKQbbR6833aKQqjQK57mv6Vvcjf_P) from Robotistan and the [CuteBot Pro Smart Car Kit](https://wiki.elecfreaks.com/en/microbit/microbit-smart-car/microbit-smart-cutebot-pro/) from Elecfreaks was used as teaching tools. Both are well supported by MicroBlocks via custom libraries included and were very well received by the students. It wasn't long before all of them started writing code to implement many ideas they had and were happily sharing them with their student peers. Smart Car kit was a favorite and they had no problem controlling it using an IR Remote control program they wrote. Even though we had to stop the class after four hours, they were still going at it and playing with the kits and having a great time.

MAKE Santa Fe was very happy with the outcome, comments by the students, and their parents. The teacher candidates were also very happy with the experience and felt confident to continue the [classes in the near future](https://makesantafe.org/event/intro-to-robotics-explore-the-world-of-physical-computing/).

![MAKE SantaFe Schedule](MAKE2024Schedule.jpg)

Overall, MAKE Santa Fe engagement was a perfect alignment of capable personnel and perfect product making it possible to create and deliver a very popular training program for young learners. 
