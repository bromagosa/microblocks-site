#### MacOS Setup ####

Go to the [Download](download) page and click the **Download** button.

Open the Downloads folder on your computer. Double-click MicroBlocks.app.zip to unzip it.

Right-click on **MicroBlocks.app** and select **Open**.
A dialog will inform you that the app was downloaded from the Internet.

<img src="assets/img/md/get-started/mac-download-warning.png" width="400">

Click the **Open** button.

Although it will seem that nothing happened, you can now open the app by double-clicking it.

To finish, drag MicroBlocks.app to the Applications folder and drag MicroBlocks.app.zip to the trash.

#### Troubleshooting ####

If you cannot open MicroBlocks.app, open **System Preferences...** from the Apple menu and  select **Security & Privacy**. In the **General** tab, make sure that **Allow apps downloaded from:** is set to **App Store and identified developers**.
