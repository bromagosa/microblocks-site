#### Windows Setup ####

Go to the [Download](download) page and click the **Download** button.

In Chrome or Edge, you will be warned that the downloaded file could harm your computer.

<img src="assets/img/md/get-started/win-downloadblocked-1.png" width="400">

Click the three-dot menu and select **Keep**.

<img src="assets/img/md/get-started/win-downloadblocked-2.png" width="400">

You will then get another warning. Click **Show more** and **Keep anyway**.

<img src="assets/img/md/get-started/win-downloadblocked-3.png" width="360">

Open the saved file, **microBlocks setup.exe**.

After one more warning, that will open the installer.

<img src="assets/img/md/get-started/win-setup-wizard.png" width="360">

Click through the installer screens to install the app.

The installer will launch MicroBlocks when it is done.
It will also add a shortcut icon to the Desktop that you can use to run MicroBlocks later.
