#### Activity Cards

Have ideas for your own activity cards? We encourage you to use the MicroBlocks formats! You can copy the style of our [official activity cards](https://drive.google.com/drive/folders/1BxsJ-nEC1MxT67O2bfrHBS9b8nmuMDad) in Google Slides/Docs format under the Creative Commons CC BY-SA 4.0 license using this [template](https://docs.google.com/presentation/d/1NYn_RX_OrrrJJO6j5mOKRSDriC7CMYItpkGHszJXXPw).

If you want your own cards to be listed under our [card gallery](learn), please get in touch with us at [interest@microblocks.fun](mailto:interest@microblocks.fun).

#### MicroBlocks Logo Use

However, the bunny image, the name *MicroBlocks*, and the phrase *Small, Fast, Human Friendly* are registered US trademarks and cannot be used without written permission from our fiscal sponsor, the [Software Freedom Conservancy](https://sfconservancy.org). Those trademarks (including the bunny image) should not appear on your own original materials or activity cards, although they should appear on direct translations of MicroBlocks activity cards.
